This is the file used to deploy a stack based on two nodes in AWS.

For the distributed file system I have used rexray/efs Docker plugin with AWS EFS support.

https://rexray.readthedocs.io/en/v0.9.0/user-guide/docker-plugins

Be sure to open the necessary port in EC2 console. 